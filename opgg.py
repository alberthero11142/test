# -*- coding:utf-8 -*-
import requests
import json
import pygsheets
from pygsheets import Chart
import pandas as pd


def GetChampionID_dict():
    champion_dict = dict()
    
    url = "https://op.gg/api/v1.0/internal/bypass/meta/champions?hl=zh_TW"
    headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control": "max-age=0",
        "cookie": "_ab=false; __qca=I0-545300778-1678073789267; _ab=false; _clck=7v6rud|1|f9o|0; __gads=ID=083372443426fef1:T=1678073729:S=ALNI_MadZoXhVCV0VFbaIxQDKXT5Q8JBtA; __gpi=UID=00000bd25c07395d:T=1678073729:RT=1678073729:S=ALNI_MZsYTznRSwjMSSquj83v1sMCi_seQ; _gid=GA1.2.416952978.1678073729; _pbjs_userid_consent_data=3524755945110770; _au_1d=AU1D-0100-001678073743-K0KK4Q11-32WW; _ublock=1; _lr_retry_request=true; _lr_env_src_ats=false; _au_last_seen_pixels=eyJhcG4iOjE2NzgwNzM3NDMsInR0ZCI6MTY3ODA3Mzc0MywicHViIjoxNjc4MDczNzQzLCJydWIiOjE2NzgwNzM3NDMsInRhcGFkIjoxNjc4MDczNzQzLCJhZHgiOjE2NzgwNzM3NDMsImdvbyI6MTY3ODA3Mzc0Mywib3BlbngiOjE2NzgwNzM3NDMsInNtYXJ0IjoxNjc4MDczNzQzLCJzb24iOjE2NzgwNzM3NDMsInBwbnQiOjE2NzgwNzM3NzQsImFkbyI6MTY3ODA3Mzc3NCwibWVkaWFtYXRoIjoxNjc4MDczNzc0LCJ0YWJvb2xhIjoxNjc4MDczNzc0LCJpbXByIjoxNjc4MDczNzc0LCJiZWVzIjoxNjc4MDczNzc0LCJ1bnJ1bHkiOjE2NzgwNzM3NzR9; __hist=%5B%7B%22region%22%3A%22tw%22%2C%22summonerName%22%3A%22%E6%AB%BB%E5%B3%B6%E9%BA%BB%E8%A1%A3%E5%AD%B8%E5%A7%90%22%7D%2C%7B%22region%22%3A%22tw%22%2C%22summonerName%22%3A%22Boringboting%22%7D%2C%7B%22region%22%3A%22tw%22%2C%22summonerName%22%3A%22%E7%B4%AB%E8%89%B2%E6%AF%8DV%22%7D%5D; cto_bidid=yCbsYF9JdXNVTyUyQk1NUkVLYXQ5UjNLJTJGZDU1a0ZiN3FHVUNmREVyS09ZckxJOXZ2Vk9QZTJ5QTBVaUw3cWJ1ZkM0M2p6Q2FtS01zN3Y1elFBUFp6Yjg1TTM3NHhzJTJGS29xZTlYSnBkMkFvOE5QTnVJWSUzRA; _awl=2.1678075558.5-d4ebc7f83aa56167b98dc31ea19f3f78-6763652d617369612d6561737431-0; _ga=GA1.2.107374568.1678073729; cto_bundle=ngoGb19yQ2RGYlMwNXpVaXZGbzBrejBLRlFxU2FSajJ4STdwU2tLYTZiRzZjNlpRYjAlMkJlMzQlMkJHTjB5ZCUyRnclMkZKWEw2byUyRlhhaWJUV1clMkJ1c2RydDclMkJ1UlhzaWQxSXkwSzBIdnZOQW0xNWljb0Y0Uk5tYm5zbTRlRiUyQm9XZFB3U3RNOEt1cGxTM2UydUFhamIwWGZYbiUyQlRGb3Zja0ElM0QlM0Q; _ga_HKZFKE5JEL=GS1.1.1678073729.1.1.1678075560.0.0.0; _ga_37HQ1LKWBE=GS1.1.1678073729.1.1.1678075560.0.0.0; _clsk=cepk4|1678076238500|20|1|u.clarity.ms/collect; _dd_s=rum=0&expire=1678077137307",
        "referer": "https://www.op.gg/summoners/tw/Boringboting",
        "sec-ch-ua": "'Chromium';v='110', 'Not A(Brand';v='24', 'Google Chrome';v='110'",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "'Windows'",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36"
    }
    response = requests.get(url, headers=headers)
    data = json.loads(response.text)["data"]
    for champion in data:
        champion_dict[champion["id"]] = champion["name"]
    return champion_dict
def GetRequiredData_FromRecent20(raw_recent20_list, champion_dict):
    result = []
    for match in raw_recent20_list:
        data = {
            "id" : match["id"],
            "name": champion_dict[match["myData"]["champion_id"]],
            "result": match["myData"]["stats"]["result"],
            "participants": [x["summoner"]["summoner_id"] for x in match["participants"]]
        }
        result.append(data)
    return result
def update_team_record_json_file(team_newrecord:list, inspect_dict:dict) -> dict:

    player_list = list(inspect_dict.keys())
    for x in ["欠女優魂我礙洨歉","李漁與綠鯉魚與驢","台北龍育賞"]:
        player_list.remove(x)
    player_list.append("茂凱")

    with open("D:/test/opgg_playerinfo.json", "r", encoding="utf-8") as jsonFile:
        file_data = json.load(jsonFile)
        team_file_record = file_data["team"]
    
    for record in team_newrecord:
        team = []
        for player in inspect_dict:
            if inspect_dict[player] in record["participants"]:
                if player in ["欠女優魂我礙洨歉","李漁與綠鯉魚與驢","台北龍育賞"]:
                    team.append("茂凱")
                else:
                    team.append(player)
        team = ",".join(team)

        if team in team_file_record:
            if record["result"] == "WIN":
                team_file_record[team]["win"] = team_file_record[team]["win"] + 1
            elif record["result"] == "LOSE":
                team_file_record[team]["lose"] = team_file_record[team]["lose"] + 1
        else:
            team_file_record[team] = dict()
            if record["result"] == "WIN":
                team_file_record[team]["win"] = 1
                team_file_record[team]["lose"] = 0
            elif record["result"] == "LOSE":
                team_file_record[team]["win"] = 0
                team_file_record[team]["lose"] = 1

        for tm in team_file_record:
            team_file_record[tm]["win_rate"] = team_file_record[tm]["win"] / (team_file_record[tm]["win"] + team_file_record[tm]["lose"])

    #sort dict
    myKeys = list(team_file_record.keys())
    myKeys.sort()
    sorted_team_file_record = {i: team_file_record[i] for i in myKeys}
    with open("D:/test/opgg_playerinfo.json", "w", encoding="utf-8") as jsonFile:
        file_data["team"] = sorted_team_file_record
        json.dump(file_data, jsonFile, indent=4, ensure_ascii=False)
    
    output = []
    for team in sorted_team_file_record:
        participant_player = [player if player in team.split(",") else "" for player in player_list]
        output.append(participant_player+[sorted_team_file_record[team]["win_rate"],sorted_team_file_record[team]["win"],sorted_team_file_record[team]["lose"]])
    team_df = pd.DataFrame(output, columns= player_list + ["winrate", "win", "lose"])
    return team_df
def update_personal_json_file(player, recent20_data:list, inspect_dict:dict):
    
    with open("D:/test/opgg_playerinfo.json", "r", encoding="utf-8") as jsonFile:
        file_data = json.load(jsonFile)
        total_cnt = file_data[player]["total_cnt"]
        File_RecentMatch = file_data[player]["RecentMatch"]


    ValidMatch_data = [match for match in recent20_data if len([val for val in match["participants"] if val in list(inspect_dict.values())]) == 5]
    match_difference = [val for val in ValidMatch_data if val not in File_RecentMatch]
    match_intersection = [val for val in ValidMatch_data if val in File_RecentMatch]

    # calculate KPI
    for new_ValidMatch in match_difference:
        champion_name = new_ValidMatch["name"]
        if champion_name not in file_data[player]["champion_statistic"]:
            file_data[player]["champion_statistic"][champion_name] = dict()
            file_data[player]["champion_statistic"][champion_name]["win"] = 0
            file_data[player]["champion_statistic"][champion_name]["lose"] = 0
            file_data[player]["champion_statistic"][champion_name]["win_rate"] = 0


        if new_ValidMatch["result"] == "WIN":
            file_data[player]["win"] = file_data[player]["win"] + 1
            file_data[player]["champion_statistic"][champion_name]["win"] = file_data[player]["champion_statistic"][champion_name]["win"] + 1
        elif new_ValidMatch["result"] == "LOSE":
            file_data[player]["lose"] = file_data[player]["lose"] + 1
            file_data[player]["champion_statistic"][champion_name]["lose"] = file_data[player]["champion_statistic"][champion_name]["lose"] + 1


        file_data[player]["win_rate"] = str(round(((file_data[player]["win"])/((file_data[player]["win"]) + (file_data[player]["lose"])))*100, 2)) + "%"
        file_data[player]["champion_statistic"][champion_name]["win_rate"] = str(round(((file_data[player]["champion_statistic"][champion_name]["win"])/((file_data[player]["champion_statistic"][champion_name]["win"]) + (file_data[player]["champion_statistic"][champion_name]["lose"])))*100, 2)) + "%"
    RecentMatch = match_difference + match_intersection
    file_data[player]["total_cnt"] = total_cnt + len(match_difference)
    file_data[player]["RecentMatch"] = RecentMatch

    with open("D:/test/opgg_playerinfo.json", "w", encoding="utf-8") as jsonFile:
        json.dump(file_data, jsonFile, indent=4, ensure_ascii=False)

    return match_difference
def update_mowcai3in1_json():
    # 茂凱3和1
    with open("D:/test/opgg_playerinfo.json", "r", encoding="utf-8") as jsonFile:
        file_data = json.load(jsonFile)

    # 重製 避免累積舊資料
    file_data["茂凱3和1"] = {
                            "total_cnt": 0,
                            "win": 0,
                            "lose": 0,
                            "win_rate": 0,
                            "champion_statistic": {
                        
                            }
                        }

    duplicate = ["欠女優魂我礙洨歉", "李漁與綠鯉魚與驢", "台北龍育賞"]
    for dupli in duplicate:
        file_data["茂凱3和1"]["win"] += file_data[dupli]["win"]
        file_data["茂凱3和1"]["lose"] += file_data[dupli]["lose"]
        file_data["茂凱3和1"]["total_cnt"] += file_data[dupli]["total_cnt"]

        for champion_name in file_data[dupli]["champion_statistic"]:

            if champion_name not in file_data["茂凱3和1"]["champion_statistic"]:
                file_data["茂凱3和1"]["champion_statistic"][champion_name] = dict()
                file_data["茂凱3和1"]["champion_statistic"][champion_name]["win"] = 0
                file_data["茂凱3和1"]["champion_statistic"][champion_name]["lose"] = 0
                file_data["茂凱3和1"]["champion_statistic"][champion_name]["win_rate"] = 0

            file_data["茂凱3和1"]["champion_statistic"][champion_name]["win"] += file_data[dupli]["champion_statistic"][champion_name]["win"]
            file_data["茂凱3和1"]["champion_statistic"][champion_name]["lose"] += file_data[dupli]["champion_statistic"][champion_name]["lose"]
            file_data["茂凱3和1"]["champion_statistic"][champion_name]["win_rate"] = str(round(((file_data["茂凱3和1"]["champion_statistic"][champion_name]["win"] / (file_data["茂凱3和1"]["champion_statistic"][champion_name]["win"] + file_data["茂凱3和1"]["champion_statistic"][champion_name]["lose"])))*100, 2)) + "%"



    file_data["茂凱3和1"]["win_rate"] = str(round(((file_data["茂凱3和1"]["win"] / file_data["茂凱3和1"]["total_cnt"]))*100, 2)) + "%"


    with open("D:/test/opgg_playerinfo.json", "w", encoding="utf-8") as jsonFile:
        json.dump(file_data, jsonFile, indent=4, ensure_ascii=False)

    return

def statistics(inspect_dict:dict):
    overview_output = []
    team_newrecord = []
    champion_dict = GetChampionID_dict()
    
    for player in inspect_dict:
        player_id = inspect_dict[player]
        # url2 = f'https://www.op.gg/summoners/tw/{name}'
        recent20_url = f"https://op.gg/api/v1.0/internal/bypass/games/tw/summoners/{player_id}?&limit=20&hl=zh_TW&game_type=flexranked"

        headers = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7",
            "cache-control": "max-age=0",
            "cookie": "_ab=false; __qca=I0-545300778-1678073789267; _ab=false; _clck=7v6rud|1|f9o|0; __gads=ID=083372443426fef1:T=1678073729:S=ALNI_MadZoXhVCV0VFbaIxQDKXT5Q8JBtA; __gpi=UID=00000bd25c07395d:T=1678073729:RT=1678073729:S=ALNI_MZsYTznRSwjMSSquj83v1sMCi_seQ; _gid=GA1.2.416952978.1678073729; _pbjs_userid_consent_data=3524755945110770; _au_1d=AU1D-0100-001678073743-K0KK4Q11-32WW; _ublock=1; _lr_retry_request=true; _lr_env_src_ats=false; _au_last_seen_pixels=eyJhcG4iOjE2NzgwNzM3NDMsInR0ZCI6MTY3ODA3Mzc0MywicHViIjoxNjc4MDczNzQzLCJydWIiOjE2NzgwNzM3NDMsInRhcGFkIjoxNjc4MDczNzQzLCJhZHgiOjE2NzgwNzM3NDMsImdvbyI6MTY3ODA3Mzc0Mywib3BlbngiOjE2NzgwNzM3NDMsInNtYXJ0IjoxNjc4MDczNzQzLCJzb24iOjE2NzgwNzM3NDMsInBwbnQiOjE2NzgwNzM3NzQsImFkbyI6MTY3ODA3Mzc3NCwibWVkaWFtYXRoIjoxNjc4MDczNzc0LCJ0YWJvb2xhIjoxNjc4MDczNzc0LCJpbXByIjoxNjc4MDczNzc0LCJiZWVzIjoxNjc4MDczNzc0LCJ1bnJ1bHkiOjE2NzgwNzM3NzR9; __hist=%5B%7B%22region%22%3A%22tw%22%2C%22summonerName%22%3A%22%E6%AB%BB%E5%B3%B6%E9%BA%BB%E8%A1%A3%E5%AD%B8%E5%A7%90%22%7D%2C%7B%22region%22%3A%22tw%22%2C%22summonerName%22%3A%22Boringboting%22%7D%2C%7B%22region%22%3A%22tw%22%2C%22summonerName%22%3A%22%E7%B4%AB%E8%89%B2%E6%AF%8DV%22%7D%5D; cto_bidid=yCbsYF9JdXNVTyUyQk1NUkVLYXQ5UjNLJTJGZDU1a0ZiN3FHVUNmREVyS09ZckxJOXZ2Vk9QZTJ5QTBVaUw3cWJ1ZkM0M2p6Q2FtS01zN3Y1elFBUFp6Yjg1TTM3NHhzJTJGS29xZTlYSnBkMkFvOE5QTnVJWSUzRA; _awl=2.1678075558.5-d4ebc7f83aa56167b98dc31ea19f3f78-6763652d617369612d6561737431-0; _ga=GA1.2.107374568.1678073729; cto_bundle=ngoGb19yQ2RGYlMwNXpVaXZGbzBrejBLRlFxU2FSajJ4STdwU2tLYTZiRzZjNlpRYjAlMkJlMzQlMkJHTjB5ZCUyRnclMkZKWEw2byUyRlhhaWJUV1clMkJ1c2RydDclMkJ1UlhzaWQxSXkwSzBIdnZOQW0xNWljb0Y0Uk5tYm5zbTRlRiUyQm9XZFB3U3RNOEt1cGxTM2UydUFhamIwWGZYbiUyQlRGb3Zja0ElM0QlM0Q; _ga_HKZFKE5JEL=GS1.1.1678073729.1.1.1678075560.0.0.0; _ga_37HQ1LKWBE=GS1.1.1678073729.1.1.1678075560.0.0.0; _clsk=cepk4|1678076238500|20|1|u.clarity.ms/collect; _dd_s=rum=0&expire=1678077137307",
            "referer": "https://www.op.gg/summoners/tw/Boringboting",
            "sec-ch-ua": "'Chromium';v='110', 'Not A(Brand';v='24', 'Google Chrome';v='110'",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "'Windows'",
            "sec-fetch-dest": "document",
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "sec-fetch-user": "?1",
            "upgrade-insecure-requests": "1",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36"
        }
        response = requests.get(recent20_url, headers=headers)

        recent20_data = json.loads(response.text)["data"]
        recent20_data = GetRequiredData_FromRecent20(recent20_data, champion_dict)

        match_difference = update_personal_json_file(player, recent20_data, inspect_dict)
        
        for match in match_difference:
            if match["id"] not in [rec["id"] for rec in team_newrecord]:
                team_newrecord.append(match)

        with open("D:/test/opgg_playerinfo.json", "r", encoding="utf-8") as jsonFile:
            file_data = json.load(jsonFile)
            overview_output.append((player, file_data[player]["win_rate"], file_data[player]["win"], file_data[player]["lose"]))
    
    # 茂凱3和1
    update_mowcai3in1_json()
    with open("D:/test/opgg_playerinfo.json", "r", encoding="utf-8") as jsonFile:
        file_data = json.load(jsonFile)
        overview_output.append(("茂凱3和1", file_data["茂凱3和1"]["win_rate"], file_data["茂凱3和1"]["win"], file_data["茂凱3和1"]["lose"]))


    team_df = update_team_record_json_file(team_newrecord, inspect_dict)

    overview_df = pd.DataFrame(overview_output, columns=["player", "winrate", "win", "lose"])

    overview_df["sort"] = overview_df["win"] / (overview_df["win"] + overview_df["lose"])
    overview_df = overview_df.sort_values('sort', ascending=False)
    overview_df = overview_df.drop('sort', axis=1)

    return overview_df, team_df


if __name__ == '__main__':

    inspect_dict = {"紫色母V":"H7o_OxrEpmv7vUqsvxLVEtsXwMdNW3x_CSw5FEgkEoxoc76GHSqp6cs8ZA",
                    "tempfhpdjkudfqaq":"2CuNoWFT6FbenzAMRAksK3Sf9gGSfofgVYxABNemxbY8fQM4ChelnZSybA",
                    "星詠者OuO":"nDTosrhHJXYq3R0Sz3PjjsYrxbBCBf9m8MgcyWamL35DCBks5mE6AYiZpQ",
                    "縈色母V":"DhtAXY0qDShovmx9Gh8ukT6HEcrqsrrWAF6rYam2p-JL4HOaUwHe8v_Hug",
                    "瘊呆小鴨":"9XhSZU9xqcjDD8MPFYWmq0TmZxzlpPcnqU0ipqHo_x9UCtCKX1GVQ0t73w",
                    "櫻島麻衣學姐":"sbJXj-jos_vjERLoBBJwyUgM2UT1kT474XZKXvfpgc1EdcJtjc16LLAtsg",
                    "欠女優魂我礙洨歉":"9t6PWvDkDWNdJeaalKEY-WsH--Siuy3RIhyONOqcYBc0md3fFJwd3MX7cg",
                    "李漁與綠鯉魚與驢":"c7MahKovjT3IBwPrSZnc5khYE3s3e9w1RD8cNiaHVpu8MA5tLgjNkYQbnA",
                    "台北龍育賞":"e6eTh0Jn1eIcuTAmDYLXPDUOpWaRXj-TvEjzLWUHCemczrKWQSU9XmxCQg"}

    overview_df, team_df = statistics(inspect_dict)
    print(overview_df)



    
    gc = pygsheets.authorize(service_file='D:/test/tactile-vial-379900-2345c338b0fd.json')
    sht = gc.open_by_url("https://docs.google.com/spreadsheets/d/1qZ2Li5YvCqXiHold0R4OetCf1ogRQYFcUc9Qptrdm5U/edit#gid=0")
    wks = sht.worksheet_by_title("overview")
    wks.set_dataframe(overview_df, 'A1')
    wks = sht.worksheet_by_title("team")
    wks.set_dataframe(team_df, 'A1')

    with open("D:/test/opgg_playerinfo.json", "r", encoding="utf-8") as jsonFile:
        file_data = json.load(jsonFile)

    for player in list(inspect_dict.keys()) + ["茂凱3和1"]:
        player_df = []
        cham_cnt = len(file_data[player]["champion_statistic"])
        for cham in file_data[player]["champion_statistic"]:
            player_df.append((cham, file_data[player]["champion_statistic"][cham]["win_rate"], file_data[player]["champion_statistic"][cham]["win"], file_data[player]["champion_statistic"][cham]["lose"]))
        player_df = pd.DataFrame(player_df, columns=["champion", "winrate", "win", "lose"])
        
        
        try:
            wks = sht.worksheet_by_title(player) 
            sheet = sht.worksheet('title',player)
            sht.del_worksheet(sheet)
        except:
            pass
        sht.add_worksheet(player)
        
        wks = sht.worksheet_by_title(player)  
        wks.set_dataframe(player_df, 'A1')
        wks.add_chart(('A2', f'A{1+cham_cnt}'), [('B2', f'B{1+cham_cnt}')], 'WinRate Chart')
