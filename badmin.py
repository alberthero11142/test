#coding=UTF-8
import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
import calendar
import time
import copy
import random
from os import listdir
import re

def get_valid_proxy_ip():
    print("-------------get valid proxy ip")
    response = requests.get("https://www.sslproxies.org/")
    
    proxy_ips = re.findall('\d+\.\d+\.\d+\.\d+:\d+', response.text)  #「\d+」代表數字一個位數以上
    
    valid_ips = []
    for index, ip in enumerate(proxy_ips):
        try:
            if index <= 30:  #驗證30組IP
                result = requests.get('https://ip.seeip.org/jsonip?',
                                    proxies={'http': ip, 'https': ip},
                                    timeout=5)
                print(result.json())
                
                return {'http': ip, 'https': ip}

        except:
            print(f"{ip} invalid")


# 台北體育館
def taipei_gym():

    Line_msg = []

    Line_msg.append("台北體育館:\n")


    cookies = {
        'PHPSESSID': 'hs1eevdp2k567b57j8nvc9cjvc',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'Connection': 'keep-alive',
        # 'Cookie': 'PHPSESSID=hs1eevdp2k567b57j8nvc9cjvc',
        'Origin': 'https://sports.tms.gov.tw',
        'Referer': 'https://sports.tms.gov.tw/venues/sschedule.php?V=49&G=1',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
        'charset': 'utf-8',
        'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    data = {
        'FUNC': 'GetVenuesScheduleTable',
        'VSN': '49',
        'Category': '1',
        'YM': '',
    }

    response = requests.post('https://sports.tms.gov.tw/venues/ajax.php', cookies=cookies, headers=headers, data=data)
    response = response.text
    # response = response.encode('utf-8').decode('unicode_escape')
    response = json.loads(response)




    tmp_list = []

    for key, value in response.items():
        if key == "Table":
            response = value
    soup = BeautifulSoup(response, "html.parser")

    result = soup.find_all("div", class_="ScheduleThGroup")
    for block in result:
        single_block_list = str(block).split("></div><div")

        for single_block in single_block_list:
            if "可租借時段" in single_block and ("六" in single_block or "日" in single_block):
                single_block = single_block.split("=")
                single_block = single_block[-2]
                single_block = single_block.replace("\"","")
                single_block = single_block.replace("v","")
                single_block = single_block.replace("可租借時段 - ","")
                single_block = single_block.strip()
                tmp_list.append(single_block)


    tmp_list.sort()
    for _ in range(len(tmp_list)):
        tmp_list[_] = tmp_list[_].split(")")


    i = 0
    summay_tmp_list = []


    while i < len(tmp_list) -1:

        if tmp_list[i] == tmp_list[i+1]:
            i += 1

        else:
            date = f"{tmp_list[0][0]})"
            start_time = ((tmp_list[0][1].split("~"))[0]).strip()
            end_time = ((tmp_list[0][1].split("~"))[1]).strip()
            summay_tmp_list.append({"date":date,"start_time":start_time,"end_time":end_time,"emptyCNT":i+1})
            tmp_list = tmp_list[i+1:]
            i = 0
    
    if len(tmp_list) != 0:
        date = f"{tmp_list[0][0]})"
        start_time = ((tmp_list[0][1].split("~"))[0]).strip()
        end_time = ((tmp_list[0][1].split("~"))[1]).strip()
        summay_tmp_list.append({"date":date,"start_time":start_time,"end_time":end_time,"emptyCNT":len(tmp_list)})


        result_dict = dict()

        for date_time_emptyCNT_info in summay_tmp_list:
            DATE = date_time_emptyCNT_info["date"]
            if DATE in result_dict:
                del date_time_emptyCNT_info["date"]
                result_dict[DATE].append(date_time_emptyCNT_info)
            else:
                result_dict[DATE] = []
                del date_time_emptyCNT_info["date"]
                result_dict[DATE].append(date_time_emptyCNT_info)


        for key,value in result_dict.items():

            tmp_connect_time = []
            now = datetime.now()
            localtime = now.strftime("%Y-%m-%d")
            key_tmp = key.split("(")[0].strip()

            sys_date = time.strptime(localtime, "%Y-%m-%d")
            booking_date =  time.strptime(key_tmp, "%Y-%m-%d")

            if sys_date > booking_date:
                continue

            if len(value) == 1:
                continue
            else:
                for i in range(1,len(value)):
                    if value[i-1]['start_time'] == "06:00" or value[i-1]['start_time'] == "07:00":
                        continue

                    if value[i]['start_time'] == value[i-1]['end_time']:
                        start = value[i-1]['start_time']
                        end = value[i]['end_time']
                        nums = min(value[i-1]['emptyCNT'],value[i]['emptyCNT'])
                        tmp_connect_time.append(f"{start} ~ {end}  {nums} 組")

        if len(tmp_connect_time) != 0:
            Line_msg.append(key)
            Line_msg = Line_msg + tmp_connect_time

    return Line_msg


# 台北市運動中心
def taipei_sport():

    proxy=get_valid_proxy_ip()

    Line_msg = []

    Line_msg.append("台北市運動中心:\n")
    location_dict = [
    {"LID": "BTSC", "lidName": "北投"},
    {"LID": "DASC", "lidName": "大安"},
    {"LID": "DTSC", "lidName": "大同"},
    {"LID": "JJSC", "lidName": "中正"},
    {"LID": "NGSC", "lidName": "南港"},
    {"LID": "NHSC", "lidName": "內湖"},
    {"LID": "SLSC", "lidName": "士林"},
    {"LID": "SSSC", "lidName": "松山"},
    {"LID": "WHSC", "lidName": "萬華"},
    {"LID": "WSSC", "lidName": "文山"},
    {"LID": "ZSSC", "lidName": "中山"}
    ]

    for location in location_dict:

        LID = location["LID"]
        lidName = location["lidName"]
        
        # get available day
        cookies = {
        '_culture': 'zh-TW',
        'ASP.NET_SessionId': 'vmv1fg14z4hyabgv3vkd04ly',
        'CategoryId': 'Badminton',
        'ID': 'ae4b5990-2f63-47e3-a227-66dedbfe08d6',
        }

        headers = {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            # Requests sorts cookies= alphabetically
            # 'Cookie': '_culture=zh-TW; ASP.NET_SessionId=vmv1fg14z4hyabgv3vkd04ly; CategoryId=Badminton; ID=ae4b5990-2f63-47e3-a227-66dedbfe08d6',
            'Origin': 'https://booking.tpsc.sporetrofit.com',
            'Referer': f'https://booking.tpsc.sporetrofit.com/Location/Reserve?LID={LID}&CategoryId=Badminton',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
            'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
        }

        data = {
            'LID': LID,
            'categoryId': 'Badminton',
            'start': '2022-09-26',
            'end': '2022-11-07',
        }

        for attempt in range(20):
            try:
                response = requests.post('https://booking.tpsc.sporetrofit.com/Location/findAllowBookingCalendars', 
                                    proxies=proxy, cookies=cookies, headers=headers, data=data, verify=False)
                break
            except:
                if attempt == 3:
                    Line_msg.append("ip banned")
                    return Line_msg
                else:
                    print("ip fail")
                    proxy=get_valid_proxy_ip()
                    continue
        
        response = response.text
        response = json.loads(response)

        date_to_be_checked = []

        for x in response:
            date_time = datetime.strptime(x["start"], "%Y-%m-%d")
            day_of_the_week = calendar.day_name[date_time.weekday()]

            if day_of_the_week == "Saturday" or day_of_the_week == "Sunday":
                date_to_be_checked.append(x["start"])



        # get bookable day_time


        result_dict = dict()


        for date in date_to_be_checked:
            

            cookies = {
            '_culture': 'zh-TW',
            'ASP.NET_SessionId': 'vmv1fg14z4hyabgv3vkd04ly',
            'CategoryId': 'Badminton',
            }

            headers = {
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Connection': 'keep-alive',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                # Requests sorts cookies= alphabetically
                # 'Cookie': '_culture=zh-TW; ASP.NET_SessionId=vmv1fg14z4hyabgv3vkd04ly; CategoryId=Badminton',
                'Origin': 'https://booking.tpsc.sporetrofit.com',
                'Referer': f'https://booking.tpsc.sporetrofit.com/Location/BookingList?LID={LID}&CategoryId=Badminton&UseDate={date}',
                'Sec-Fetch-Dest': 'empty',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Site': 'same-origin',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
                'X-Requested-With': 'XMLHttpRequest',
                'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': '"Windows"',
            }

            params = {
                'LID': LID,
                'categoryId': 'Badminton',
                'useDate': date,
            }

            data = {
                '_search': 'false',
                'nd': '1663935725636',
                'rows': '200',
                'page': '1',
                'sidx': '',
                'sord': 'asc',
            }


            for attempt in range(20):
                try:
                    response = requests.post('https://booking.tpsc.sporetrofit.com/Location/findAllowBookingList',
                                proxies=proxy, params=params, cookies=cookies, headers=headers, data=data, verify=False)
                    break
                except:
                    if attempt == 3:
                        Line_msg.append("ip banned")
                        return Line_msg
                    else:
                        print("ip fail")
                        proxy=get_valid_proxy_ip()
                        continue
            
            response = response.text
            response = json.loads(response)


            tmp_list = []


            if "rows" not in response:
                continue


            for row in response["rows"]:

                if row["Status"] == "預約":
                    
                    tmp_list.append([row["StartTime"]["Hours"], row["EndTime"]["Hours"]])
                    






            i = 0
            summay_tmp_list = []


            while i < len(tmp_list) -1:
                tmp_list
                if tmp_list[i] == tmp_list[i+1]:
                    i += 1

                else:

                    summay_tmp_list.append({"start_time":tmp_list[0][0],"end_time":tmp_list[0][1],"emptyCNT":i+1})
                    tmp_list = tmp_list[i+1:]
                    i = 0
                    
            if len(tmp_list) != 0:

                summay_tmp_list.append({"start_time":tmp_list[0][0],"end_time":tmp_list[0][1],"emptyCNT":len(tmp_list)})




            if len(summay_tmp_list) != 0:

                result_dict[date] = summay_tmp_list





        for key,value in result_dict.items():

            if len(value) == 1:

                tmp_connect_time = []
                
                if value[0]['end_time'] - value[0]['start_time'] == 2:
                    if value[0]['start_time'] < 8:
                        continue
                    else:
                        if value[0]['start_time'] < 10:
                            start = f"0{value[i-1]['start_time']}:00"
                        else:
                            start = f"{value[i-1]['start_time']}:00"

                        if value[0]['end_time'] < 10:
                            end = f"0{value[i]['end_time']}:00"
                        else:
                            end = f"{value[i]['end_time']}:00"

                        nums = value[0]['emptyCNT']
                        tmp_connect_time.append(f"{start} ~ {end}  {nums} 組")

                if len(tmp_connect_time) != 0:

                    if f"{lidName}:" not in Line_msg:
                        Line_msg.append(f"{lidName}:")

                    date_time = datetime.strptime(key, "%Y-%m-%d")
                    day_of_the_week = calendar.day_name[date_time.weekday()]
                    if day_of_the_week == "Saturday":
                        Line_msg.append(f"{key} ( 六 )")
                    elif day_of_the_week == "Sunday":
                        Line_msg.append(f"{key} ( 日 )")


                    Line_msg = Line_msg + tmp_connect_time




            else:
                tmp_connect_time = []
                for i in range(1,len(value)):

                    if value[i]['start_time'] < 8:
                        continue
                    
                    if value[i]['end_time'] - value[i]['start_time'] == 2:

                        if value[i]['start_time'] < 10:
                            start = f"0{value[i]['start_time']}:00"
                        else:
                            start = f"{value[i]['start_time']}:00"

                        if value[i]['end_time'] < 10:
                            end = f"0{value[i]['end_time']}:00"
                        else:
                            end = f"{value[i]['end_time']}:00"

                        nums = value[i]['emptyCNT']
                        tmp_connect_time.append(f"{start} ~ {end}  {nums} 組")
                        
                        continue


                    if value[i]['start_time'] == value[i-1]['end_time']:
                        
                        if value[i-1]['start_time'] < 10:
                            start = f"0{value[i-1]['start_time']}:00"
                        else:
                            start = f"{value[i-1]['start_time']}:00"

                        if value[i]['end_time'] < 10:
                            end = f"0{value[i]['end_time']}:00"
                        else:
                            end = f"{value[i]['end_time']}:00"

                        nums = min(value[i-1]['emptyCNT'],value[i]['emptyCNT'])
                        tmp_connect_time.append(f"{start} ~ {end}  {nums} 組")

                if len(tmp_connect_time) != 0:

                    if f"{lidName}:" not in Line_msg:
                        Line_msg.append(f"{lidName}:")

                    date_time = datetime.strptime(key, "%Y-%m-%d")
                    day_of_the_week = calendar.day_name[date_time.weekday()]
                    if day_of_the_week == "Saturday":
                        Line_msg.append(f"{key} ( 六 )")
                    elif day_of_the_week == "Sunday":
                        Line_msg.append(f"{key} ( 日 )")


                    Line_msg = Line_msg + tmp_connect_time
    return Line_msg


# 新北運動中心
def new_taipei():
    
    Line_msg = []

    Line_msg.append("新北運動中心:\n")

    location_dict = [
    {"LID": "[8]", "lidName": "三重"},
    {"LID": "[28]", "lidName": "中和"},
    {"LID": "[27]", "lidName": "新店"},
    {"LID": "[3]", "lidName": "永和"}
    ]

    for location in location_dict:

        LID = location["LID"]
        lidName = location["lidName"]


        headers = {
                    'authority': 'api.sportable.tw',
                    'accept': 'application/json, text/plain, */*',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'origin': 'https://sportable.tw',
                    'referer': 'https://sportable.tw/',
                    'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"',
                    'sec-fetch-dest': 'empty',
                    'sec-fetch-mode': 'cors',
                    'sec-fetch-site': 'same-site',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
                }

        params = {
                'sport_id': '1',
                'district_ids': LID,
                }

        response = requests.get('https://api.sportable.tw/api/v2/search/courts/schedule', params=params, headers=headers)
        response = response.text
        response = json.loads(response)

        response = response["schedules"]
        


        result_dict = dict()

        for row in response:

            timeArray = time.gmtime(int(row["date"])+86400)
            styleTime = time.strftime(str("%Y-%m-%d"), timeArray)
            date_time = datetime.strptime(styleTime, "%Y-%m-%d")
            day_of_the_week = calendar.day_name[date_time.weekday()]

            if day_of_the_week != "Saturday" and day_of_the_week != "Sunday":
                continue

            result_dict[row["date"]] = []

            for seg_info in row["hours"]:

                start = int(seg_info["hour"])
                end = start + 1

                if start < 8:
                    continue

                if start < 10:
                    start = f"0{start}:00"
                else:
                    start = f"{start}:00"
                
                if end < 10:
                    end = f"0{end}:00"
                else:
                    end = f"{end}:00"

                result_dict[row["date"]].append({"start_time":start,"end_time":end,"emptyCNT":len(seg_info["data"][0]["timeslotId"])})
        
        result_dict = dict(sorted(result_dict.items()))
        key_list = copy.deepcopy(list(result_dict.keys()))

        for key in key_list:

            timeArray = time.gmtime(int(key)+86400)
            styleTime = time.strftime(str("%Y-%m-%d"), timeArray)

            result_dict[styleTime] = result_dict[key]
            del result_dict[key]


        for key,value in result_dict.items():

            if len(value) == 1:
                continue
            else:
                tmp_connect_time = []
                for i in range(1,len(value)):
                    if value[i]['start_time'] == value[i-1]['end_time']:
                        start = value[i-1]['start_time']
                        end = value[i]['end_time']
                        nums = min(value[i-1]['emptyCNT'],value[i]['emptyCNT'])
                        tmp_connect_time.append(f"{start} ~ {end}  {nums} 組")

            if len(tmp_connect_time) != 0:

                if f"{lidName}:" not in Line_msg:
                    Line_msg.append(f"{lidName}:")

                date_time = datetime.strptime(key, "%Y-%m-%d")
                day_of_the_week = calendar.day_name[date_time.weekday()]

                if day_of_the_week == "Saturday":
                    Line_msg.append(f"{key} ( 六 )")
                elif day_of_the_week == "Sunday":
                    Line_msg.append(f"{key} ( 日 )")

                Line_msg = Line_msg + tmp_connect_time

    return Line_msg



if __name__ == '__main__':

    now = datetime.now()
    hm = datetime.strftime(now,'%H:%M')
    m = datetime.strftime(now,'%M')

# 早安系統
    if hm == "07:00" or hm == "07:01":
    
        # LINE Notify 權杖
        token = 'HeHeROWzDM0QK28aaYkokldebz0QROftKgAAB55sCXc'

        # 要發送的訊息
        message = "\n" + "帥哥美女早安"

        # HTTP 標頭參數與資料
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': message }

        # 以 requests 發送 POST 請求
        requests.post("https://notify-api.line.me/api/notify",
            headers = headers, data = data)
        
# 圖片系統
    if m == "00" or m == "01":
        
        mypath = "D:/test/pic"
        files = listdir(mypath)
        name = random.choice(["睿宇", "繼睿", "玉林"])
        pic_name = random.choice(files)
        print(pic_name)

        with open("D:/test/name_recoed.txt","r") as f:
            name_record = f.read()
        print(name_record)
        name_record = name_record.split(",")
        new_record = []

        for record in name_record:
            if name in record:
                tmp = record.split(":")
                tmp[1] = str(int(tmp[1]) + 1)
                tmp = ":".join(tmp)
                new_record.append(tmp)
            else:
                new_record.append(record)

        new_record = ",".join(new_record)

        with open("D:/test/name_recoed.txt","w") as f:
            f.write(new_record)

        # LINE Notify 權杖
        token = 'HeHeROWzDM0QK28aaYkokldebz0QROftKgAAB55sCXc'


        # HTTP 標頭參數與資料
        headers = { "Authorization": "Bearer " + token }
        
        # HTTP 標頭參數與資料
        headers = { "Authorization": "Bearer " + token }
        
        message = "\n" + f"{name}~\n{new_record}"
        image = open(f'{mypath}/{pic_name}', 'rb')

        data = { 'message': message }
        files = { 'imageFile': image }

        # 以 requests 發送 POST 請求
        requests.post("https://notify-api.line.me/api/notify",
            headers = headers, data = data, files = files)

# 場地資訊系統

    Line_msg_total = []
    Line_msg_total = Line_msg_total + taipei_gym()
    Line_msg_total.append("============")
    Line_msg_total = Line_msg_total + taipei_sport()
    Line_msg_total.append("============")
    Line_msg_total = Line_msg_total + new_taipei()
    Line_msg_total.append("============")
    

    Line_msg_total = "\n".join(Line_msg_total)
    print(Line_msg_total)

    with open("D:/test/badmin_last_record.txt","r") as f:
        old_record = f.read()

    if old_record != Line_msg_total or m == "00" or m == "01":

        with open("D:/test/badmin_last_record.txt","w") as f:
            f.write(Line_msg_total)


        # LINE Notify 權杖
        token = 'HeHeROWzDM0QK28aaYkokldebz0QROftKgAAB55sCXc'

        # 要發送的訊息
        message = "\n" + Line_msg_total

        # HTTP 標頭參數與資料
        headers = { "Authorization": "Bearer " + token }
        data = { 'message': message }

        # 以 requests 發送 POST 請求
        requests.post("https://notify-api.line.me/api/notify",
            headers = headers, data = data)

