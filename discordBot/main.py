from discord.ext import commands
import discord
import json
import asyncio
import requests
from bs4 import BeautifulSoup
import os

# Construct the full path to the config.json file
config_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.json')

with open(config_path, "r", encoding = "utf8") as file:
    data = json.load(file)

my_token = data['token']


# intents是要求機器人的權限
intents = discord.Intents.all()

# command_prefix是前綴符號，可以自由選擇($, #, &...)
bot = commands.Bot(command_prefix = "/", intents = intents)

# 黑名單
death_note = {
                575328666440040451:"櫻島",
                389063640751538176:"偉智"
            }
# 782251425031520257:"嘎嘎"
# 618062694221742095:"GOOD一BANANA"
# 445943815736524821:"Ornement_TW"


# 各功能初始狀態
disconnecter_enabled = True
zipper_enabled = True

# 偉志 PTT 發文稽查參數
scrape_url = "https://www.pttweb.cc/user/purplemumi"
seen_urls = set(data['seen_urls'])


# Group function
class StatusGroup(discord.app_commands.Group):
    @discord.app_commands.command(description="Show Status")
    async def show_status(self, interaction: discord.Interaction):
        global bot_msg_channel

        msg = []
        if disconnecter_enabled:
            msg.append("Disconnecter 'kill_mode'")
        else:
            msg.append("Disconnecter 'mercy_mode'")
        if zipper_enabled:
            msg.append("Zipper 'kill_mode'")
        else:
            msg.append("Zipper 'mercy_mode'")

        await interaction.response.send_message("\n".join(msg), ephemeral=False)
        return

class DisconnecterGroup(discord.app_commands.Group):
    @discord.app_commands.command(description="Disconnecter 'kill_mode'")
    async def kill_mode(self, interaction: discord.Interaction):
        global bot_msg_channel
        global disconnecter_enabled

        if interaction.user.id not in death_note:
            disconnecter_enabled = True
            await interaction.response.send_message("Disconnecter 'kill_mode'", ephemeral=False)
            return
        
        return
    
    @discord.app_commands.command(description="Disconnecter 'mercy_mode'")
    async def mercy_mode(self, interaction: discord.Interaction):
        global bot_msg_channel
        global disconnecter_enabled

        if interaction.user.id not in death_note:
            disconnecter_enabled = False
            await interaction.response.send_message("Disconnecter 'mercy_mode'", ephemeral=False)
            return
        return

class ZipperGroup(discord.app_commands.Group):
    @discord.app_commands.command(description="Zipper 'kill_mode'")
    async def kill_mode(self, interaction: discord.Interaction):
        global bot_msg_channel
        global zipper_enabled

        if interaction.user.id not in death_note:
            zipper_enabled = True
            await interaction.response.send_message("Zipper 'kill_mode'", ephemeral=False)
            return
        
        return
    
    @discord.app_commands.command(description="Zipper 'mercy_mode'")
    async def mercy_mode(self, interaction: discord.Interaction):
        global bot_msg_channel
        global zipper_enabled

        if interaction.user.id not in death_note:
            zipper_enabled = False
            await interaction.response.send_message("Zipper 'mercy_mode'", ephemeral=False)
            return
        return
    
@bot.event
async def on_ready():

    server_id = 761534507161092096

    global normal_msg_channel
    normal_msg_channel_id = 761534507161092098
    normal_msg_channel = bot.get_channel(normal_msg_channel_id)

    global bot_msg_channel
    bot_msg_channel_id = 1148082846272061481
    bot_msg_channel = bot.get_channel(bot_msg_channel_id)

    print(f"目前登入身份 --> {bot.user}")

    #-----------------Start the web scraping loop#-----------------
    bot.loop.create_task(scrape_website())
    #---------------------------------------------------

    #-----------------add slash command-----------------
    disconnecterGroup = DisconnecterGroup(name="disconnecter",
                                      description="Auto kick blacklist member from voice channel"
                                      )
    zipperGroup = ZipperGroup(name="zipper",
                                      description="Auto delete blacklist member msg"
                                      )
    statusGroup = StatusGroup(name="status",
                                      description="Status function"
                                      )
    bot.tree.add_command(disconnecterGroup)
    bot.tree.add_command(zipperGroup)
    bot.tree.add_command(statusGroup)
    #---------------------------------------------------


    await bot_msg_channel.send("Lee_yu Killer Restart")
    await bot_msg_channel.send("Disconnecter 'kill_mode'")
    await bot_msg_channel.send("Zipper 'kill_mode'")

    bot.tree.copy_global_to(guild=discord.Object(server_id))
    await bot.tree.sync(guild=discord.Object(server_id))

@bot.event
async def scrape_website():
    global seen_urls  # Make sure to use the global seen_urls variable

    while True:
        try:
            # Send a GET request to the URL
            response = requests.get(scrape_url)

            # Parse the HTML content of the page
            soup = BeautifulSoup(response.text, 'html.parser')

            # Find all elements with class attribute "thread-item e7-container"
            thread_items = soup.find_all(class_="thread-item e7-container")

            # Iterate through the found elements and extract the URLs
            for thread_item in thread_items:
                a_tag = thread_item.find('a', href=True)
                if a_tag:
                    thread_url = "https://www.pttweb.cc" + a_tag['href']
                    # Check if the URL is new
                    if thread_url not in seen_urls:
                        seen_urls.add(thread_url)
                        await normal_msg_channel.send(thread_url)

            # Save the updated seen_urls list back to the config file
            data['seen_urls'] = list(seen_urls)
            with open(config_path, 'w', encoding='utf8') as file:
                json.dump(data, file, indent=4)

            # Sleep for a period (e.g., 1 hour) before checking again
            await asyncio.sleep(3600)  # 3600 seconds = 1 hour

        except Exception as e:
            print(f'Error: {str(e)}')

@bot.event
async def on_voice_state_update(member, before, after):

    global bot_msg_channel
    global disconnecter_enabled

    if disconnecter_enabled:
        if (member.id in death_note.keys()):

            if before.channel != after.channel:
                await member.edit(voice_channel=None)
                await bot_msg_channel.send(f"{member.mention} Goodbye")

@bot.event
async def on_message(message):

    global bot_msg_channel
    global zipper_enabled

    if zipper_enabled:
        if (message.author.id in death_note.keys()):
            await message.delete()
            await bot_msg_channel.send(f"{message.author.mention} you have no discourse right")


bot.run(my_token)